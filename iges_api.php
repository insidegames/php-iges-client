<?php
require 'vendor/autoload.php';
Guzzle\Http\StaticClient::mount();
use Guzzle\Http\Client;

final class IgesAPI
{
  const IGES_ROOT = 'http://iges.pl';
  private $_api_url  = IGES_ROOT.'/api/';
  private $_api_auth = IGES_ROOT.'/oauth/token';
  private $_accept   = 'application/vnd.iges+json;version=1';
  private static $oInstance = false;

  public static function getInstance()
  {
    if( self::$oInstance == false )
    {
      self::$oInstance = new IgesAPI();
    }
    return self::$oInstance;
  }

  public function __construct()
  {
    $this->loadConfigs();
    $this->getAccessToken();
    $this->client = new Client($this->_api_url);
    $this->setHeaders();
  }

  private function setHeaders()
  {
    $this->client->setDefaultOption('headers', array(
      'Accept' => $this->_accept,
      'Authorization' => "Bearer {$this->app_access_token}"
    ));
  }

  // THIS SHOULD BE ADJUSTED TO YOUR NEEDS
  private function loadConfigs()
  {
    $this->api_key    = Configuration::get('igesconnect_key');
    $this->api_secret = Configuration::get('igesconnect_secret');
  }

  // DITTO
  public function logMessage($message)
  {
    return p($message);
  }

  private function getAccessToken()
  {
    $response = Guzzle::post($this->_api_auth, array(
      'body' => array(
        'grant_type'    => 'client_credentials',
        'client_id'     => $this->api_key,
        'client_secret' => $this->api_secret
      ))
    );

    $response = $response->json();

    if( !empty( $response['access_token'] ) )
      $this->app_access_token = $response['access_token'];
    else
      $this->logMessage(array('message' => 'Could not get IGES Access Token', 'response' => $response));
  }

  public function get($url, $params)
  {
    $params = is_array($params) ? '?'.http_build_query($params) : '';
    $request = $this->client->get("{$url}{$params}");
    return $this->makeRequest($request);
  }

  public function post($url, $params)
  {
    $request = $this->client->post($url);
    $request->setBody($params);
    return $this->makeRequest($request);
  }

  private function makeRequest($request)
  {
    try {
      $response = $this->client->send($request);
      return $response->json();
    } catch (Guzzle\Http\Exception\BadResponseException $e) {
      if ((int)$e->getResponse()->getStatusCode() == 401){
        $this->getAccessToken();
        $this->setHeaders();
        $this->makeRequest($request);
      } else if ((int)$e->getResponse()->getStatusCode() == 404)
        return false;
      else
        $this->logMessage($e->getResponse());
    }
  }

}

?>
