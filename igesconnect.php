<?php
require 'iges_api.php';

class IgesConnect
{
  private $customer = NULL; // this should be handler to your user model

  private function createDbFields()
  {
    Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'customer` ADD COLUMN iges_id INT(10) NULL AFTER `date_upd`;');
    Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'customer` ADD COLUMN iges_token VARCHAR(64) NULL AFTER `iges_id`;');
    return true;
  }

  private function findPlayerByEmail()
  {
    return IgesAPI::getInstance()->post('app/players/find', array('email' => $this->customer->email));
  }

  private function createPlayerProfile()
  {
    $c = $this->customer;
    $params = array(
      'email' => $c->email,
      'first_name' => $c->firstname,
      'last_name' => $c->lastname,
      'gender' => $c->gender == 1 ? 'm' : 'f',
      'display_name' => "{$c->firstname} {$c->lastname}"
    );
    return IgesAPI::getInstance()->post('app/players', $params);
  }

  private function pushEvent($event_name, $player_iges_id, $param = NULL)
  {
    $params = array(
      'event_id' => $event_name,
      'param' => $param
    );
    return IgesAPI::getInstance()->post("app/players/{$player_iges_id}/event", $params);
  }

  public function examplePlayerLogin()
  {
    if(! isset($this->customer->iges_id) ){
      $player = $this->findPlayerByEmail();
      if( !$player ) {
         $player = $this->createPlayerProfile();
         $this->pushEvent('Register', $player['id']);
      }
      $this->customer->iges_id = $player['id'];
      $this->customer->iges_token = $player['token'];
      $this->customer->save();
    }
    $this->pushEvent('Daily_visit', $this->customer->iges_id);
  }
}
?>
